<!--
SPDX-FileCopyrightText: 2023 Arbetsförmedlingen JobTech - The Swedish Public Employment Service's Investment in JobTech Development

SPDX-License-Identifier: CC0-1.0
-->

[![Arbetsförmedlingens logo](https://arbetsformedlingen.se/webdav/files/logo/logo.svg)](https://arbetsformedlingen.se)


# JobTech CI

This program is used for builds and deploys at JobTech.

It uses the Gitlab CI/CD functionality.

[![Bild på versionsnummer](https://img.shields.io/badge/dynamic/json?color=green&label=Version%20%28commit%20hash%29&query=%24%5B0%5D.short_id&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F43503997%2Frepository%2Fcommits%3Fref_name%3Dmaster)](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/commits/master)

This software is developed at JobTech, a part of The Swedish Public Employment Agency.


## About the new version
We have done a major upgrade, from version 0 to version 1.

 - The heavy lifting of the pipeline is handled by [GitLab Auto
   DevOps](https://docs.gitlab.com/ee/topics/autodevops/) and [Cloud
   Native Buildpacks](https://buildpacks.io/) instead of running the
   pipeline commands in the CI code explicitly.
 - The deploy buttons are now generated dynamically to match the
   application's infra code, meaning you don't have to see irrelevant
   buttons.

Please note that it is still new and could contain bugs. Please try it
out and report.

## Installation

### For application developers

 1. Verify that `Enable shared runners for this project` is active in the project Settings -> CI/CD -> Runners section.

 2. Add a file `.gitlab-ci.yml` in the root folder of the **application repo** (NOT infra-repo) with this content:
```
include:
- remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v2.1.6/jobtech-ci.yml'
```
In some projects (including Python projects with a `requirement.txt`),
the underlying buildpack will have problems finding and executing the
automatic tests of the project, whereby the job `test` will fail. Then
add this to `.gitlab-ci.yml`:
```
variables:
  TEST_DISABLED: 1
```
(note. what matter is that it is defined - the value seems to be ignored by the buildpack)

To program the execution of automatic tests, create a new job in `stage: test` (the job must NOT be
called `test` as this will cause a name clash with buildpack), like this example (adjust
to your programming language/test framework):
```
testjob:
  stage: test
  image: python:3.11
  script:
    - pip install -r test-requirements.txt
    # the repository files are checked out and available already
    - python -m unittest hello_world_test.py > testresult.txt
  artifacts:
    paths:
    - testresult.txt
```

3a. If the application should be deployed, verify that an infra repo exists as a sibling to the application repo, with the same name but with `-infra` as additional suffix. It should contain at least a `develop` overlay.

3b. If the application should not be deployed, add this to the variable list in your `gitlab-ci.yml`:
```
variables:
  DEPLOYS_DISABLED: 1
```

#### Summary of configuration variables
 - `TEST_DISABLED`: set to disable the test stage.
 - `DEPLOYS_DISABLED`: set to 1 if your app does not have an infra repo.
 - `DEPLOY_FROM_ALL_BRANCHES`: set to 1 if you want deploy buttons from pipelines from all branches.


### For Python Poetry applications
See [this README](python-poetry/README.md) for specific instructions.


### For Gitlab administrators

Create an access token and configure CD/CD with it:
 - Navigate to the organisation top level, Settings -> Access tokens
 - Create a new token with the following settings:
   - Name: jobtech-ci (please use this to facilitate later administration)
   - Role: Maintainer
   - Scopes: write_repository
   - You don't need to have an expiry date, but you may want to.

 - Copy the new token into your your top level organisation Settings -> CI/CD -> Variables,
   create a new variable called `ACCESS_TOKEN`, paste the token from above into the value field,
   and ***IMPORTANTLY check the Masked checkbox***, then save.
   - Protected: selected
   - Expand variable reference: selected

## Usage

### Build an image

When you push to your application repo, the CI pipeline will
start. You can follow its process in the Gitlab GUI, in the repo's
CI/CD pipeline view.

If the app repo has a corresponding infra repo, its short commit sha
will be written to your application's infra repo, the develop overlay.

To deploy to your various environments that you have defined in your
infra overlays, press the corresponding deploy buttons in the CD/CD pipeline
view. Then the CI system will make a change to the version specified in
the corresponding overlay.


## ArgoCD notifications
Please check the [separate documentation page](docs/argocd-notifications.md).

## Known issues

## Support

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Contributing

Please have a look at the [CONTRIBUTING](CONTRIBUTING.adoc) document.

----

## License

GNU GENERAL PUBLIC LICENSE Version 3 - see the [LICENSE](LICENSE) file for details

----

## Maintainers

Name and git-account for primary maintainer/s:

Per Weijnitz @perweij
Joakim Verona @jave

## Credits and References

- [Gitlab CI](https://docs.gitlab.com/ee/ci/)
- [Examples](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/) .
