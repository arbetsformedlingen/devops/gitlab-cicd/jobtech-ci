# Python Poetry CI Configuration

This GitLab CI configuration file, [`python-poetry/jobtech-ci.yml`](python-poetry/jobtech-ci.yml), helps Python Poetry applications create a static binary of their application in a scratch image. The CI process generates a Dockerfile automatically, so you don't need to have one in your repository.

## How to Use

To use this CI setup for your Python Poetry application, follow these steps:

1. Remove any existing Dockerfile from your repository.
2. Copy the example below and paste it into your `.gitlab-ci.yml` file.

```yml
include:
  remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/cdabc87/python-poetry/jobtech-ci.yml'

variables:
  # DEPLOYS_DISABLED:             1 # disable deploy buttons - optional
  TEST_DISABLED:                  1 # disable buildpack test job - recommended

  PYTHON_POETRY_PYTHON_VERSION:   3.12
  PYTHON_POETRY_ARTIFACT_NAME:    main
  PYTHON_POETRY_FILES_TO_PACKAGE: $PYTHON_POETRY_ARTIFACT_NAME.py settings.py
  PYTHON_POETRY_DIRS_TO_PACKAGE:  src
  PYTHON_POETRY_OS_PKGS_BUILD:    bash
  PYTHON_POETRY_OS_PKGS_TEST:     jq
  PYTHON_POETRY_ENV:              LANG=C

test_example:
  stage: test
  services:
    - $DOCKER_SVC
  image:
    name: $DOCKER_IMG
  script:
  - apk add jq
  - sh tests/test_functional.sh docker run --rm -i "$BRANCH_IMAGE"
```

3. Customize the variables and test job settings to fit your application's specific requirements.

By following these steps, you can easily set up CI for your Python Poetry application without the need for a separate Dockerfile.


## Running automatic tests

Create a script `tests/test.sh` where the tests are run - the script
will be executed in a test layer in the generated Dockerfile.
This is an example of such a script:
```
#!/bin/sh

python -m unittest -v || exit 1
```

You can also add additional tests into the `.gitlab-ci.yml`. You can
use this pattern and modify the `script` section (the image variables
used here are set in the environment):
```
test_app:
  stage: test
  services:
    - $DOCKER_SVC
  image:
    name: $DOCKER_IMG
  script:
  - apk add jq
  - echo "some input" | docker run --rm -i "$BRANCH_IMAGE" | jq .status > output.txt
  - grep "success" output.txt || exit 1
```
Of course you could put that script content in a file under `tests/`
and execute that instead, instead of entering the script into the CI
file.


## Löcal build
You can build locally like this. Please note that you only need to provide
the parameters to `create-dockerfile.sh` where its default values
are not sufficient. This example simply shows the flags for all parameters:
```
# get the build script and the Dockerfile generation script
curl -LO https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v1.3.1/python-poetry/build.sh
curl -LO https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v1.3.1/python-poetry/create-dockerfile.sh

# generate the Dockerfile. Adjust the arguments to suit your needs below - not all are needed
sh create-dockerfile.sh
    -n "main"                # the name of the final program binary
    -v "3.12"                # Python version
    -f "main.py settings.py" # the files needed for the program to work
    -d "src"                 # the directories needed
    -t "jq bash"             # the alpine packages needed for the tests to work
    -p "gcc"                 # the alpine packages needed for the build towork
    -e '["./main"]'          # the Dockerfile's entrypoint spec
    -x "LANG=C FOO=bar"      # the environment variables needed by the program
    | tee Dockerfile

# build
podman build -t my-img .
```

If `build.sh` or `create-dockerfile.sh` already exists in the repo, those
files override the default scripts in the CI pipeline.
