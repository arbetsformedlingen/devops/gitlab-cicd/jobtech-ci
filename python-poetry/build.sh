#!/bin/sh

# Initialize variables
PYTHON_POETRY_ARTIFACT_NAME=""
PYTHON_POETRY_MAIN_PROGRAM=""
PYTHON_POETRY_DIRS_TO_PACKAGE=""
OS_PKGS=""
PY_PKGS=""

print_usage() {
    echo "Usage: $0 --artifact-name <name> [--os-pkg <pkg name>]... [--py-pkg <python package name>]..."
    exit 1
}

# Parse command-line arguments
while [ $# -gt 0 ]; do
    case "$1" in
        --artifact-name)
            if [ -n "$2" ] && [ "${2#--}" = "$2" ]; then
                PYTHON_POETRY_ARTIFACT_NAME="$2"
                shift 2
            else
                echo "Error: --artifact-name requires a value"
                print_usage
            fi
            ;;
        --main-program)
            if [ -n "$2" ] && [ "${2#--}" = "$2" ]; then
                PYTHON_POETRY_MAIN_PROGRAM="$2"
                shift 2
            else
                echo "Error: --main-program requires a value"
                print_usage
            fi
            ;;
        --code-dir)
            if [ -n "$2" ] && [ "${2#--}" = "$2" ]; then
                PYTHON_POETRY_DIRS_TO_PACKAGE="$PYTHON_POETRY_DIRS_TO_PACKAGE $2"
                shift 2
            else
                echo "Error: --main-program requires a value"
                print_usage
            fi
            ;;
        --os-pkg)
            if [ -n "$2" ] && [ "${2#--}" = "$2" ]; then
                OS_PKGS="$OS_PKGS $2"
                shift 2
            else
                echo "Error: --os-pkg requires a value"
                print_usage
            fi
            ;;
        --py-pkg)
            if [ -n "$2" ] && [ "${2#--}" = "$2" ]; then
                PY_PKGS="$PY_PKGS $2"
                shift 2
            else
                echo "Error: --py-pkg requires a value"
                print_usage
            fi
            ;;
        *)
            echo "Error: Unknown argument: $1"
            print_usage
            ;;
    esac
done

# Trim leading and trailing whitespace
OS_PKGS="$(echo "$OS_PKGS" | xargs)"
PY_PKGS="$(echo "$PY_PKGS" | xargs)"

# Display parsed variables
echo "PYTHON_POETRY_ARTIFACT_NAME: $PYTHON_POETRY_ARTIFACT_NAME"
echo "OS_PKGS: $OS_PKGS"
echo "PY_PKGS: $PY_PKGS"



DEPS_UBUNTU="${DEPS_UBUNTU:-} $OS_PKGS" #fixme
DEPS_FEDORA="${DEPS_FEDORA:-} $OS_PKGS" #fixme
DEPS_ALPINE="${DEPS_ALPINE:-python3 py3-pip scons build-base patchelf} $OS_PKGS"



# Function to detect the Linux distribution
detect_distro() {
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        echo "$ID"
    else
        echo "unknown"
    fi
}



install_deps() {
    distro=$(detect_distro)

    case "${distro}" in
        ubuntu)
            if [ -n "$DEPS_UBUNTU" ] ; then
                apt-get update -y
                apt-get install -y $DEPS_UBUNTU
            fi
            ;;
        fedora)
            if [ -n "$DEPS_FEDORA" ] ; then
                dnf install $DEPS_FEDORA
            fi
            ;;
        alpine)
            if [ -n "$DEPS_ALPINE" ] ; then
                apk update
                apk add $DEPS_ALPINE
            fi
            ;;
        *)
            echo "Unsupported Linux distribution: ${distro}" >&2
            exit 1
            ;;
    esac
}




install_deps

python -m venv venv
. venv/bin/activate

pip install poetry
pip install setuptools
pip install wheel
pip install staticx
pip install pyinstaller

if [ -n "$PY_PKGS" ]; then
    pip install $PY_PKGS
fi

poetry install

echo COMMAND: pyinstaller -F $(echo -ne "$PYTHON_POETRY_DIRS_TO_PACKAGE" | tr ' ' '\n' | xargs -I'{}' echo -n "--collect-all {} ") "$PYTHON_POETRY_MAIN_PROGRAM" >&2

pyinstaller -F $(echo -ne "$PYTHON_POETRY_DIRS_TO_PACKAGE" | tr ' ' '\n' | xargs -I'{}' echo -n "--collect-all {} ") "$PYTHON_POETRY_MAIN_PROGRAM"

staticx dist/"$(basename $PYTHON_POETRY_MAIN_PROGRAM)" "$PYTHON_POETRY_ARTIFACT_NAME"
strip "$PYTHON_POETRY_ARTIFACT_NAME"
