#!/bin/sh
set -o errexit
set -o nounset

PYTHON_POETRY_ARTIFACT_NAME="main"
PYTHON_POETRY_MAIN_PROGRAM="main.py"
PYTHON_POETRY_PYTHON_VERSION="3.12"
PYTHON_POETRY_ASSET_FILES=""
PYTHON_POETRY_FILES_TO_PACKAGE="$PYTHON_POETRY_ARTIFACT_NAME.py"
PYTHON_POETRY_DIRS_TO_PACKAGE=""
PYTHON_POETRY_PKGS_BUILD=""
PYTHON_POETRY_OS_PKGS_BUILD=""
PYTHON_POETRY_OS_PKGS_TEST=""
PYTHON_POETRY_ENTRYPOINT='["./'"$PYTHON_POETRY_ARTIFACT_NAME"'"]'
PYTHON_POETRY_ENV=""

# Parse command line arguments
while getopts "m:n:v:f:d:t:y:p:e:x:a:" opt; do
  case $opt in
    n) PYTHON_POETRY_ARTIFACT_NAME=$OPTARG;;
    m) PYTHON_POETRY_MAIN_PROGRAM=$OPTARG;;
    v) PYTHON_POETRY_PYTHON_VERSION=$OPTARG;;
    f) PYTHON_POETRY_FILES_TO_PACKAGE=$OPTARG;;
    d) PYTHON_POETRY_DIRS_TO_PACKAGE=$OPTARG;;
    y) PYTHON_POETRY_PKGS_BUILD=$OPTARG;;
    p) PYTHON_POETRY_OS_PKGS_BUILD=$OPTARG;;
    t) PYTHON_POETRY_OS_PKGS_TEST=$OPTARG;;
    e) PYTHON_POETRY_ENTRYPOINT=$OPTARG;;
    x) PYTHON_POETRY_ENV=$OPTARG;;
    a) PYTHON_POETRY_ASSET_FILES=$OPTARG;;
    \?) echo "Invalid option -$OPTARG" >&2; exit 1;;
  esac
done


BASEIMG=""
case $PYTHON_POETRY_PYTHON_VERSION in
    3.12*) BASEIMG="docker.io/alpine:3.20.0";;
    3.11*) BASEIMG="docker.io/alpine:3.19.1";;
    3.10*) BASEIMG="docker.io/alpine:3.17.7";;
    *) echo "bad python version $PYTHON_POETRY_PYTHON_VERSION" >&2; exit 1;;
esac


cat << EOF
FROM $BASEIMG as build

WORKDIR /app
COPY $PYTHON_POETRY_FILES_TO_PACKAGE poetry.lock pyproject.toml README.md build.sh /app
EOF

for D in $PYTHON_POETRY_DIRS_TO_PACKAGE; do
    echo "COPY $D /app/$D/"
done

cat << EOF
RUN sh build.sh --main-program $PYTHON_POETRY_MAIN_PROGRAM --artifact-name $PYTHON_POETRY_ARTIFACT_NAME $(echo "$PYTHON_POETRY_PKGS_BUILD" | tr ' ' '\n' | xargs -I'{}' echo -n "--py-pkg {} ") $(echo "$PYTHON_POETRY_OS_PKGS_BUILD" | tr ' ' '\n' | xargs -I'{}' echo -n "--os-pkg {} ") $(echo "$PYTHON_POETRY_DIRS_TO_PACKAGE" | tr ' ' '\n' | xargs -I'{}' echo -n "--code-dir {} ")
RUN rm -rf /tmp/*



###############################################################################
# test
FROM build AS test
# When tests have run successfully a file is created.
# In the next step this file is copied, if the tests weren't successful and no file was created,
# the build will fail.
EOF

if [ -n "$PYTHON_POETRY_OS_PKGS_TEST" ]; then
    echo "RUN apk add $PYTHON_POETRY_OS_PKGS_TEST"
fi

if [ -d tests ]; then
    cat <<EOF
COPY tests/ tests/

# some projects use python -m pytest -v tests
RUN  . venv/bin/activate && if [ -f tests/test.sh ]; then { python -m poetry install --with tests || true ; } && sh tests/test.sh; fi && touch /.tests-successful
EOF
fi

cat <<EOF



###############################################################################
# Runtime
FROM scratch

# If tests weren't successful the copy will fail so that the build fails
COPY --from=test /.tests-successful /
COPY --from=build /app/$PYTHON_POETRY_ARTIFACT_NAME .
COPY --from=build /tmp /tmp
EOF

for F in $PYTHON_POETRY_ASSET_FILES; do
    echo "COPY $F /app/"
done

if [ -n "$PYTHON_POETRY_ENV" ]; then
    echo "ENV $PYTHON_POETRY_ENV"
fi

cat <<EOF
ENTRYPOINT $PYTHON_POETRY_ENTRYPOINT
EOF
