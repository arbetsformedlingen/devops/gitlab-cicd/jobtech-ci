variables:
  JOBTECH_CI_VER: patch/better-check-for-deploys-disabled
  JOBTECH_CI_VER: v1.0.1


include:
- template: Auto-DevOps.gitlab-ci.yml
- remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v2.1.1/security2.yml'



# configure the entire pipeline to not run in merge request pipelines
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE != 'merge_request_event'




variables:
  DOCKER_IMG:   docker:26.1.1
  DOCKER_SVC:   docker:26.1.1-dind
  GIT_IMG:      bitnami/git:2.44.0
  JSONNET_IMG:  bitnami/jsonnet:0.20.0
  HADOLINT_IMG: hadolint/hadolint:v2.12.0-debian
  MEGALINT_IMG: ghcr.io/oxsecurity/megalinter-java:v7.3.0
  JQ_IMG:       ghcr.io/jqlang/jq:1.7.1

  # Use new builder
  AUTO_DEVOPS_BUILD_IMAGE_CNB_BUILDER: "heroku/builder:22"

  CI_DEBUG_TRACE: "true"

  # Git configuration
  GIT_DEPTH: 1                                  # Create a shallow copy

  BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA

  # DEPLOYS_DISABLED: 1

  # Control autodevops
  # BROWSER_PERFORMANCE_DISABLED: 1
  # CODE_INTELLIGENCE_DISABLED: 1
  # CODE_QUALITY_DISABLED: 1
  # CONTAINER_SCANNING_DISABLED: 1
  DAST_DISABLED: 1
  # DEPENDENCY_SCANNING_DISABLED: 1
  # LOAD_PERFORMANCE_DISABLED: 1
  # REVIEW_DISABLED: 1
  # SAST_DISABLED: 1
  # SECRET_DETECTION_DISABLED: 1
  # https://gitlab.com/gitlab-org/gitlab-foss/-/issues/47651:
  TEST_DISABLED: 1
  #DOCKERFILE_LINT_DISABLED: 1



# This list of stages is copied from Gitlab Autodevops and extended with the marked stages.
# The reason we need to do a full copy is that existing lists cannot be modified.
stages:
  - build
  - test
  - generate  # Our stage to generate button code
  - execute   # Our stage to execute the generated code
  - deploy
  - review
  - dast
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - cleanup




#### RULES ####




dockerfile-lint:
  stage: test
  image: $DOCKER_IMG
  services:
    - $DOCKER_SVC
  script:
    - HADOLINT="docker run -v $PWD:$PWD:z --rm --workdir $PWD -i $HADOLINT_IMG hadolint"
    - if [ `find . -name 'Dockerfile*' | wc -l` -eq 0 ]; then echo "No Dockerfiles found in repo, nothing to lint." >&2; exit; fi
    - mkdir -p reports
    - find . -type f -name 'Dockerfile*' -exec sh -c "$HADOLINT"' --format gitlab_codeclimate - < $0 > reports/$(echo $0 | sha256sum | cut -d " " -f1).json' {} \;
    - JQ="docker run -v $PWD:$PWD:z --rm --workdir $PWD -i $JQ_IMG"
    - find reports/ -type f -exec $JQ -s 'add' {} + > reports/gl-code-quality-report.json
  artifacts:
    reports:
      codequality: reports/gl-code-quality-report.json
  rules:
    - if: $DOCKERFILE_LINT_DISABLED != '1' && $DOCKERFILE_LINT_DISABLED != 'TRUE' && $DOCKERFILE_LINT_DISABLED != 'true'



#megalint:
#  stage: test
#  image: $DOCKER_IMG
#  services:
#    - $DOCKER_SVC
#  script:
#    |
#    CHECKOUT_DIR=$RUNNER_TEMP_PROJECT_DIR
#    rm -rf $CHECKOUT_DIR/jobtech-ci
#    mkdir -p reports
#    GIT="docker run -v $CHECKOUT_DIR:$CHECKOUT_DIR:z --rm --workdir $CHECKOUT_DIR -i $GIT_IMG git"
#    $GIT clone --depth 1 https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci.git $CHECKOUT_DIR/jobtech-ci
#    $CHECKOUT_DIR/jobtech-ci/src/code_quality.sh > reports/megalint.txt
#  artifacts:
#    paths:
#    - reports/megalint.txt



# Explanation of these rules below:
#  - .gen_overlays_file
#  - generate buttons
#  - deploy buttons
#
# Objective: automatically determine which deploy buttons should
# be visible, based on what overlays are found in the current repo's
# corresponding -infra-repo. In a previous version of CI, the buttons
# needed to be hardcoded.
#
# Method:
#  1. 'generate buttons' executes the script defined in '.gen_overlays_file', with the
#  purpose of dynamically generate a job artifact 'jobs.yml' - this file is a Gitlab CI
#  file describing a set of deploy buttons.
#  2. 'deploy buttons' imports the artifact 'jobs.yml' and Gitlab CI executes its content.
#
# Please note that 'jobs.yml' includes loads some helper rules from a JobTech CI
# file called 'deploy.yml' found in this repo.
#

.gen_overlays_file: &gen_overlays_file |
  GIT="docker run -v $PWD:$PWD:z --rm --workdir $PWD -i $GIT_IMG git"
  JSONNET="docker run -v $PWD:$PWD:z --rm --workdir $PWD -i $JSONNET_IMG"

  clone_url=$(echo "$CI_REPOSITORY_URL" | sed 's|.git$|-infra.git|')

  if ! default_branch=`$GIT ls-remote "$clone_url" | grep -E '(master|main)'`; then
      echo "**** ERROR" >&2
      echo "**** ERROR No infrarepo found" >&2
      echo "**** ERROR" >&2
      exit 1
  fi

  infra_branch=$($GIT ls-remote "$clone_url" | grep -E '(master|main)' | sed -E 's|^.*/([^/]*)$|\1|')
  if $GIT clone -b "$infra_branch" "$clone_url" infra; then

    # Verify that there is a develop overlay
    if [ ! -f infra/kustomize/overlays/develop/kustomization.yaml ]; then
      echo "**** ERROR: no develop overlay found in $clone_url" >&2
      exit 1
    fi

    # No need to go further if there is only a develop overlay
    if [ `find infra/kustomize/overlays/ -name kustomization.yaml | wc -l` = 1 ]; then
      cat << "EOSCRIPT" > gen-msg.jsonnet
        local infomsg(msg) =
          {
            stage: "deploy",
            script: "echo " + msg
          };

        {
          ['info']: infomsg("No deploy buttons because there is only a develop overlay in the infra repo.")
        }
        {
          include: {
            remote: "https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/master/deploy.yml"
          }
        }
  EOSCRIPT
      $JSONNET - < gen-msg.jsonnet >jobs.yml
      exit
    fi

    # Create sweet jsonnet array of the overlay names, e.g. ['staging', 'prod']
    if find infra/kustomize/overlays/ -name kustomization.yaml \
       | sed -e 's|^.*overlays/||' -e "s|^\([^/]*\)/.*$|\1|" \
       | grep -v '^develop$' \
       | awk '{printf "\x27%s\x27, ", $0}' \
       | sed 's/, $//' \
       | awk '{printf "[%s]", $0}' \
       > overlays.jsonnet
    then
      echo -n "Compiled list of overlays: " >&2
      cat overlays.jsonnet  >&2
    else
      echo "**** ERROR: something went wrong when compiling a list of overlays for $clone_url" >&2
      exit 1
    fi
  else
      echo "**** ERROR" >&2
      echo "**** ERROR Cloning infrarepo failed" >&2
      echo "**** ERROR" >&2
      exit 1
  fi

  cat << "EOSCRIPT" > gen-buttons.jsonnet
    local job(overlay) =
      {
        extends: ".deploy",
        variables: {
          OVERLAY: overlay
        }
      };

    local err(msg) =
      {
        stage: "deploy",
        script: "echo " + msg
      };

    local overlays = import 'overlays.jsonnet';

    if std.length(overlays) >= 1 then
      {
        ['deploy_' + x]: job(x)
        for x in overlays
      }
      {
        include: {
          remote: "https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/master/deploy.yml"
        }
      }
    else
      {
        ['error']: err("WARNING: too few infra overlays. Provide at least one develop and one more in order to do manual deploy.")
      }
  EOSCRIPT

  echo -e "Executing jsonnet program:\n################\n#### overlays.jsonnet:" >&2
  cat overlays.jsonnet >&2
  echo "#### gen-buttons.jsonnet:" >&2
  cat gen-buttons.jsonnet >&2
  echo -e "################\n\n" >&2

  $JSONNET - < gen-buttons.jsonnet >jobs.yml

  echo "#### jobs.yml:" >&2
  cat jobs.yml >&2




generate buttons:
  stage: generate
  image: $DOCKER_IMG
  services:
    - $DOCKER_SVC
  script:
    - *gen_overlays_file
  artifacts:
    paths:
    - jobs.yml
  rules:
    - if: ( $CI_COMMIT_REF_SLUG == $CI_DEFAULT_BRANCH || $DEPLOY_FROM_ALL_BRANCHES == '1' ) && $DEPLOYS_DISABLED != '1' && $DEPLOYS_DISABLED != 'TRUE' && $DEPLOYS_DISABLED != 'true'



deploy buttons:
  stage: execute
  needs:
  - generate buttons
  trigger:
    include:
    - artifact: jobs.yml
      job: generate buttons
    strategy: depend
  rules:
    - if: ( $CI_COMMIT_REF_SLUG == $CI_DEFAULT_BRANCH || $DEPLOY_FROM_ALL_BRANCHES == '1' ) && $DEPLOYS_DISABLED != '1' && $DEPLOYS_DISABLED != 'TRUE' && $DEPLOYS_DISABLED != 'true'
