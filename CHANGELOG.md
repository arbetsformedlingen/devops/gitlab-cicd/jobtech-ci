<!--
SPDX-FileCopyrightText: 2023 Arbetsförmedlingen JobTech - The Swedish Public Employment Service's Investment in JobTech Development

SPDX-License-Identifier: CC0-1.0
-->

## 2.1.4 (2024-10-18)
- Make commit better formatted and informative

## 2.1.3 (2024-10-17)
- Use more informative commit messages

## 2.1.2 (2024-10-16)
- Introduce DEPLOY\_FROM\_ALL\_BRANCHES

## ... unfortunate gap in changelog here

## 0.3.7 (2023-05-03)
- Use CI\_JOB\_TOKEN instead of deprecated CI\_BUILD\_TOKEN

## 0.3.6 (2023-03-06)

### feature (1 change)

- [Try to generate CHANGELOG.md automatically](arbetsformedlingen/devops/gitlab-cicd/jobtech-ci@bddb081c6016dcca0653e1ad5ab5da595531c38f)

## 0.3.5 (2023-03-06)

No changes.

## [0.3.4] (2023-02-24)

- only attempt to build image when there is Dockerfile in the repo
- rename current button deploy_develop to autodeploy_develop
- add a new button for manual deploys: deploy_develop

## [0.3.3] (2023-02-21)

- only do automatic deploy to develop from the default branch

## [0.3.2] (2023-02-21)

- add i1, t2 and staging overlays

## [0.3.1] (2023-02-21)

- handle upper case letters in branch names

## [0.3.0] (2023-02-16)

- facilitate for the teams to use a common ACCESS_TOKEN

## [0.2.3] (2023-02-15)

- add inclusion of security tests
- rename jobtech-ci.yaml to jobtech-ci.yml
- add deploy_test rule

## [0.2.2] (2023-02-14)

- reuse common code in a template

## [0.2.1] (2023-02-14)

- autodetect branch in kustomize infra repo
- add protection against running manual deploys in parallel

## [0.2.0] - 2023-02-14

- The first version tracked here
