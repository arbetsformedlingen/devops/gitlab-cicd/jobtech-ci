# ArgoCD Notifications

## TLDR; Summary for developers

0. Locate your application's file in the argocd-infra repo, for example [hello-world-develop.yaml](https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/blob/main/kustomize/clusters/testing/hello-world-develop.yaml?ref_type=heads#L8)
1. Add the following information:
```
metadata:
  labels:
    environment: develop  <-- this value should match the overlay name
    gitlabid: "37845884"  <-- Copy ID from project's Settings -> General (in app repo)
  annotations:
    notifications.argoproj.io/subscribe.on-deployed.gitlab: calamari (can be any value)
```

There, now you will be able to see which version is deployed in namespace `hello-world-develop` in the testing cluster (At least one deploy must happen before it shows.)

Here you see the new Environment link on the project start page:

![](docs/imgs/environments0.png)


Clicking Environment will show you details:

![](docs/imgs/environments1.png)


### Autodeploy and Waiting for a Deployment in CI

This shows an example of a pipeline that deploys an app to the testing cluster, waits until the image is deployed and in state ready, runs tests towards that app, and deploys to the production cluster.

```
variables:
  DEPLOYS_DISABLED: 1 # Needed to disable deploy buttons and make deploys automatic

.default_rules:
  main_branch:
    - if: '$CI_COMMIT_REF_SLUG == $CI_DEFAULT_BRANCH'  # Rule to check if on the default branch

# Autodeploy to the Testing cluster
autodeploy_testing:
  stage: deploy
  image: docker:25.0.0-beta.2
  services:
    - docker:25.0.0-beta.2-dind
  resource_group: write-infra
  variables:
    OVERLAY: "testing"
  rules:
    - !reference [.default_rules, main_branch]
  script:
    - !reference [.deploy, script]

# Wait for the deployment in the Testing cluster to reach state ready
on_deployed_testing:
  extends: wait_for_deploy
  stage: deploy
  variables:
    OCP_TEST_OVERLAY: "testing"
  rules:
    - !reference [.default_rules, main_branch]
  needs: [autodeploy_testing]

# Test the autodeployed app in the Testing cluster
test_testing_deployment:
  stage: deploy
  image: alpine:latest  # Using a lightweight image
  script:
    - echo "Running tests on the testing cluster..."
    - apk add --no-cache bash
    - apk add --no-cache curl
    - ./test/api-tests.sh <testing cluster endpoint>
  rules:
    - !reference [.default_rules, main_branch]
  needs: [on_deployed_testing]

# Autodeploy to the Prod cluster
autodeploy_prod:
  stage: deploy
  image: docker:25.0.0-beta.2
  services:
    - docker:25.0.0-beta.2-dind
  resource_group: write-infra
  variables:
    OVERLAY: "prod"
  rules:
    - !reference [.default_rules, main_branch]
  script:
    - !reference [.deploy, script]
  needs: [test_testing_deployment]
```

Keep reading of you want to understand the details. Otherwise you're good to go now.

## Overview

Let's say you want to pause a CI/CD pipeline until a deploy has been
successfully performed in OpenShift.

Your CI pipeline would:
 1. Build the application image
 2. Perform basic testing of the image
 3. Update the application's infra repo with the newly built image information
 4. Wait....

How would your wait-step know when ArgoCD has detected the new image
information in the image repo, and successfully deployed this exact
image the cluster?

Answer: ArgoCD would send a webhook back to Gitlab API with the
relevant status when the deploy is finished.


## Tech

First some review of the tech involved, in case you need a brush up.

#### ArgoCD
ArgoCD monitors the infra repos of all applications, and whenever it detects a new commit, it syncs the changes to OpenShift.

This is done on two levels:
 - the configuration of each deployed application (e.g. the app [quiz-backend](https://gitlab.com/arbetsformedlingen/devops/quiz/quiz-backend/-/tree/main?ref_type=heads) has its configuration in sibling repo [quiz-backend-infra](https://gitlab.com/arbetsformedlingen/devops/quiz/quiz-backend-infra/-/tree/main?ref_type=heads))
 - the configuration of ArgoCD itself (where general ArgoCD config is stored, including a list of apps to be deployed - here is where [quiz-backend](https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/blob/main/kustomize/clusters/testing/quiz-develop.yaml?ref_type=heads) is registered)

You can check out your OpenShift cluster's ArgoCd by clicking the grid in the upper right corner:

![](docs/imgs/start_argocd.png)


#### ArgoCD notifications
ArgoCD can be configured to send webhooks containing deployment
information when the desired conditions are met. This can be used to
send email, ping chat channels, trigger external deploy systems etc.

Read more [here](https://argo-cd.readthedocs.io/en/stable/operator-manual/notifications/)

#### Gitlab API
Gitlab has powerful features for automation, including
[REST APIs](https://docs.gitlab.com/ee/api/rest/index.html) and
[GraphQL APIs](https://docs.gitlab.com/ee/api/graphql/index.html).

Read more about powerful features [here](https://docs.gitlab.com/ee/api/).

To access the APIs, access tokens are used. A good alternative to create
new API tokens in Gitlab is to use the ephemeral tokens that are automatically
provided in the Gitlab CI context (as environment variable `$CI_JOB_TOKEN`).


## Preparing your ArgoCD app config
Update your application's ArgoCD config with the fields `gitlabid` and
`notifications.argoproj.io/subscribe.*`. The asterisk should be
replaced with `<trigger>.<service>`. Its value should be a recipient
value (but the actual value is not automatically used, so it can be a
dummy value). The Gitlab project ID can be found in Gitlab project Settings -> General.

What does `trigger` and `service` mean?

Those are defined in each cluster's
[argocd-notifications-cm.yaml](https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/blob/main/kustomize/clusters/testing/argocd-notifications-cm.yaml)
(which may not exist yet, unless notifications are already used in
that cluster).


#### Services
A typical service which describes the coupling to a target host could look like this (in `argocd-notifications-cm.yaml`):
```
data:
  service.webhook.gitlab: |
    url: https://gitlab.com
    headers:
    - name: Content-type
      value: application/json
    - name: PRIVATE-TOKEN
      value: $gitlab-api-token
```

#### Triggers
A typical trigger that would fire when a successful deploy has happened could look like this (in `argocd-notifications-cm.yaml`):
```
data:
  trigger.on-deployed: |
    - description: Application is synced and healthy. Triggered once per commit.
      oncePer: app.status.sync.revision
      send:
      - app-deploy-success
      when: app.status.operationState != nil and app.status.operationState.phase in ['Succeeded'] and app.status.health.status == 'Healthy' and app.status.sync.revision == app.status.operationState.syncResult.revision
```
The value under `send` specifies a template.

#### Templates
A template is where you form the actual request to be sent. You can use golang template functionality
and ArgoCD variable interpolation in these.

Here is what a template that would call the Gitlab Environments API could look like (in `argocd-notifications-cm.yaml`):
```
data:
  template.app-deploy-success: |
    webhook:
      gitlab:
        method: POST
        path: /api/v4/projects/{{ .app.metadata.labels.gitlabid }}/deployments
        body: |
          {
             {{ $commitMessage0 := (call .repo.GetCommitMetadata .app.status.sync.revision).Message }}
             {{ $commitMessage1 := (call .strings.ReplaceAll $commitMessage0 "\n" "<NL>" ) }}
            "status": "success",
            "environment": "{{ .app.metadata.labels.environment }}",
            "sha": "{{ regexReplaceAll `.*Commit in code repo: ([0-9a-f]*)<NL>.*$$` $commitMessage1 `${1}` }}",
            "ref": "{{ .app.spec.source.targetRevision }}",
            "tag": "false"
          }
```

#### Application specific ArgoCD configuration
Changes to `argocd-notifications-cm.yaml` have impact on all applications in the cluster, so only general configs go there.

Here are the relevant parts of an example ArgoCD app specific config, which activates notifications for a Gitlab project
with a given service and trigger:
```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  labels:
    gitlabid: "54946659"
  annotations:
    # Syntax: notifications.argoproj.io/subscribe.<trigger>.<service>: <recipient>
    notifications.argoproj.io/subscribe.on-deployed.gitlab: calamari
```

## Preparing your Gitlab CI config
Only the imagination is the limit for what you can do with all this! You can use a webhook to the Gitlab Pipelines API to start a new pipeline.
In the example template above, the Gitlab Environments API is called to update the application project's [Environments and deployments](https://docs.gitlab.com/ee/ci/environments/) information.

You find your Gitlab project's environments under Operate -> Environments:


![](docs/imgs/environments.png)


Here is a sequence diagram for how this example works:


![](docs/imgs/seq.png)


We could use that to make a CI pileline sleep until a deploy has happened:
```
variables:
  OCP_TEST_OVERLAY: "develop"

include:
  - remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/master/argocd-notifications/wait-for-deploy.yml'
```

## Debugging
It can be useful to tail the notification controller's log:
```
oc project openshift-gitops
oc logs -f $(oc get pods | grep openshift-gitops-notifications-controller | awk '{ print $1 }')
```
